/******************************************************************************
* Copyright 2013-2016 LASIGE                                                  *
*                                                                             *
* Licensed under the Apache License, Version 2.0 (the "License"); you may     *
* not use this file except in compliance with the License. You may obtain a   *
* copy of the License at http://www.apache.org/licenses/LICENSE-2.0           *
*                                                                             *
* Unless required by applicable law or agreed to in writing, software         *
* distributed under the License is distributed on an "AS IS" BASIS,           *
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.    *
* See the License for the specific language governing permissions and         *
* limitations under the License.                                              *
*                                                                             *
*******************************************************************************
* Metrics for measuring similarity between collections and/or lists.          *
*                                                                             *
* @author Daniel Faria                                                        *
******************************************************************************/
package util;

import java.util.Collection;
import java.util.Map;

public class Similarity
{

	/**
	 * Computes the Jaccard similarity between two Collections of Objects
	 * @param <X>
	 * @param c1: the first Collection 
	 * @param c2: the second Collection
	 * @return the Jaccard similarity between c1 and c2
	 */
	public static <X extends Object> double jaccard(Collection<X> c1, Collection<X> c2)
	{
		if(c1.size() == 0 || c2.size() == 0)
			return 0.0;
		double intersection = 0.0;
		double union = 0.0;
		for(Object o : c1)
		{
			if(c2.contains(o))
				intersection++;
			else
				union++;
		}
		union += c2.size();
		return intersection/union;
	}
	
	/**
	 * Computes the Precision of a Collection of Objects, against a second Collection
	 * representing the control
	 * @param <X>
	 * @param c1: the first Collection 
	 * @param c2: the second Collection
	 * @return the Precision of c1 in relation to c2
	 */
	public static <X extends Object> double precision(Collection<X> c1, Collection<X> c2)
	{
		if(c1.size() == 0 || c2.size() == 0)
			return 0.0;
		double intersection = 0.0;
		for(Object o : c1)
			if(c2.contains(o))
				intersection++;
		return intersection/c1.size();
	}
	
	/**
	 * Computes the Jaccard similarity between two Maps of Objects to their quantities
	 * @param <X>
	 * @param m1: the first Map
	 * @param m2: the second Map
	 * @return the Jaccard similarity between m1 and m2
	 */
	public static <X extends Object> double quantJaccard(Map<X,Double> m1, Map<X,Double> m2)
	{
		if(m1.size() == 0 || m2.size() == 0)
			return 0.0;
		double intersection = 0.0;
		double union = 0.0;
		for(Object o : m1.keySet())
		{
			if(m2.containsKey(o))
			{
				intersection += Math.min(m1.get(o),m2.get(o));
				union += Math.max(m1.get(o),m2.get(o));
			}
			else
				union += m1.get(o);
		}
		for(Object o : m2.keySet())
		{
			if(!m1.containsKey(o))
				union += m2.get(o);
		}
		return intersection/union;
	}
	
	/**
	 * Computes the Recall of a Collection of Objects, against a second Collection
	 * representing the control
	 * @param <X>
	 * @param c1: the first Collection 
	 * @param c2: the second Collection
	 * @return the Recall of c1 in relation to c2
	 */
	public static <X extends Object> double recall(Collection<X> c1, Collection<X> c2)
	{
		if(c1.size() == 0 || c2.size() == 0)
			return 0.0;
		double intersection = 0.0;
		for(Object o : c1)
			if(c2.contains(o))
				intersection++;
		return intersection/c2.size();
	}
}