package util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class Ontology
{

//Attributes

	//The ontology classes <-> labels and the hierarchical relationships
	protected HashMap<Integer,String> classes;
	protected HashMap<String,Integer> names;
	protected HashMap<Integer,Integer> ancestors;

	private static final String TERM = "[Term]";
	private static final String ID = "id: NCBITaxon:";
	private static final String NAME = "name: ";
	private static final String SYNONYM = "synonym: ";
	private static final String IS_A = "is_a: NCBITaxon:";

//Constructors

	public Ontology(String path)
	{
        //Initialize the data structures
        classes = new HashMap<Integer,String>();
        names = new HashMap<String,Integer>();
        ancestors = new HashMap<Integer,Integer>();
		try
		{
			init(path);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

//Public Methods

	/**
	 * @param index: the id of the class to search in the map
	 * @return the list of ancestors of the given class
	 */
	public Set<Integer> getAncestors(int index)
	{
		HashSet<Integer> classAncestors = new HashSet<Integer>();
		getAncestors(index,classAncestors);
		return classAncestors;
	}

	/**
	 * @return the set of Classes in the Ontology
	 */
	public Set<Integer> getClasses()
	{
		return classes.keySet();
	}

	/**
	 * @param name: the name of the class to search in the ontology
	 * @return the index of the class with the give name
	 */
	public Integer getClass(String name)
	{
		return names.get(name);
	}

	/**
	 * @return the set of names in the Ontology
	 */
	public Set<String> getNames()
	{
		return names.keySet();
	}

	/**
	 * @param index: the index of the class to get the name
	 * @return the name of the class with the given index
	 */
	public String getName(int index)
	{
		return classes.get(index);
	}

	/**
	 * @param index: the index of the class to get the parent
	 * @return the index of the parent of the class with the given index
	 */
	private Integer getParent(int index)
	{
		return ancestors.get(index);
	}


	//Parses an OBO ontology file to populate the Ontology data structures
	private void init(String path) throws Exception
	{
		System.out.println("Reading ontology file: " + path);
		long time = System.currentTimeMillis();
		BufferedReader in = new BufferedReader(new FileReader(path));
		String line = in.readLine();
		//Skip header
		while(line != null && !line.equals(TERM))
			line = in.readLine();
		//Read one term at a time
		while(line != null)
		{
			int id = -1;
			String name = "";
			Set<String> synonyms = new HashSet<String>();
			int superClass = -1;
			while(line != null && !line.equals(""))
			{
				line = in.readLine();
				//Read the Id
				if(line.startsWith(ID))
				{
					String idString = line.substring(ID.length());
					if(idString.matches("[0-9]+"))
						id = Integer.parseInt(idString);
					else
						break;
				}
				//Read the name
				else if(line.startsWith(NAME))
					name = line.substring(NAME.length());
				else if(line.startsWith(SYNONYM))
				{
					int start = line.indexOf("\"")+1;
					int end = line.indexOf("\"",start);
					if(end > start)
						synonyms.add(line.substring(start, end));
				}
				//Read the superclass
				else if(line.startsWith(IS_A))
				{
					int start = IS_A.length();
					int end = line.indexOf(" ", start);
					String idString = line.substring(start, end);
					if(idString.matches("[0-9]+"))
						superClass = Integer.parseInt(idString);
				}
			}
			//Fill in the data structures
			if(id > -1)
			{
				classes.put(id, name);
				names.put(name, id);
				for(String s : synonyms)
					if(!names.containsKey(s))
						names.put(s,id);
				if(superClass > -1)
					ancestors.put(id, superClass);
			}
			//Move to next term
			while(line != null && !line.equals(TERM))
				line = in.readLine();
		}
		in.close();
		time = (System.currentTimeMillis() - time)/1000;
		System.out.println("Finished in " + time + " seconds");
		System.out.println("Classes: " + classes.size());
		System.out.println("Relations: " + ancestors.size());
		System.out.println();
	}

	//Recursive auxiliary method for computing the ancestors of a class
	private void getAncestors(int classId, Set<Integer> classAncestors)
	{
		if(ancestors.containsKey(classId))
		{
			Integer parent = getParent(classId);
			if(parent != null)
			{
				classAncestors.add(parent);
				getAncestors(parent, classAncestors);
			}
		}
	}
}