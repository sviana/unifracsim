package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Arrays;

import util.Ontology;
import util.Similarity;

public class Main
{
	private static Ontology ncbi;
	private static HashMap<Integer,Double> control;
	private static boolean quantitative = false;

	public static void main(String[] args) throws Exception
	{
		String sourceFile = null, controlFile = null, taxFile = null, sourceDirectory = null;

		//Process the arguments
		for(int i = 0; i < args.length; i++)
		{
			if((args[i].equalsIgnoreCase("-s") || args[i].equalsIgnoreCase("--source")) &&
					i < args.length-1)
			{
				sourceFile = args[++i];
			}
			else if((args[i].equalsIgnoreCase("-c") || args[i].equalsIgnoreCase("--control")) &&
					i < args.length-1)
			{
				controlFile = args[++i];
			}
			else if((args[i].equalsIgnoreCase("-t") || args[i].equalsIgnoreCase("--taxonomy")) &&
					i < args.length-1)
			{
				taxFile = args[++i];
			}
			else if((args[i].equalsIgnoreCase("-d") || args[i].equalsIgnoreCase("--source_directory")) &&
					i < args.length-1)
			{
				sourceDirectory = args[++i];
			}
			else if(args[i].equalsIgnoreCase("-q"))
				quantitative = true;
		}

		if(taxFile == null || controlFile == null || ( sourceDirectory != null &&  sourceFile != null) )
		{
			System.out.println("Illegal arguments ! See README.md file for options");
			System.exit(-1);
		}


		ncbi = new Ontology(taxFile);
		control = readCommunity(controlFile);

		if(sourceFile != null)
		{
			String sim = processSourceFile(sourceFile);
			System.out.println("Unifrac Similarity: " + sim);
		}
		else if(sourceDirectory != null)
		{
			processSourceDirectory(sourceDirectory);
		}
	}

	private static String processSourceFile(String sourceFile) throws Exception
	{
		HashMap<Integer,Double> source = readCommunity(sourceFile);
		String result = Similarity.jaccard(source.keySet(), control.keySet()) + "\t" +
				Similarity.precision(source.keySet(), control.keySet()) + "\t" +
				Similarity.recall(source.keySet(), control.keySet());
		if(quantitative)
			result += "\t" + Similarity.quantJaccard(source, control);
		return result;
	}

	private static void processSourceDirectory(String sourceDirectory) throws Exception
	{
		File sourceDirectoryFile = new File(sourceDirectory);
		String sourceAbsPath = sourceDirectoryFile.getAbsolutePath();
		String outputTabularFilename = sourceAbsPath.substring(0, sourceAbsPath.lastIndexOf("-"))
		+ "." + (quantitative? "":"un") + "weighted.unifrac.tsv";
		File outputTabularFile = new File( outputTabularFilename );

		String suffixToSearch = "names.txt";

		if (quantitative) {
			suffixToSearch = "qtys.tsv";
		}

		final String finalSuffix = suffixToSearch;

		System.out.println("Finding all " + finalSuffix + " files under dir " + sourceDirectoryFile.getAbsolutePath());

		File[] sourceFiles = sourceDirectoryFile.listFiles(new FilenameFilter()
		{
			@Override
			public boolean accept(File dir, String name)
			{
				return name.endsWith(finalSuffix);
			}
		});
		System.out.println("Source directory: " + sourceDirectory);
		System.out.println("Files: " + Arrays.toString(sourceFiles));

		HashMap<String,String> results = new HashMap<String,String>();
		for(File sourceFile : sourceFiles)
		{
			System.out.println("Processing: " + sourceFile.getAbsolutePath());
			if (quantitative) {
				results.put(sourceFile.getName().replace(".qtys.tsv",""), processSourceFile(sourceFile.getAbsolutePath()));
			} else {
				results.put(sourceFile.getName().replace(".names.txt",""), processSourceFile(sourceFile.getAbsolutePath()));
			}
		}

		PrintWriter out = new  PrintWriter(new FileOutputStream(outputTabularFile));
		if(quantitative)
			out.println("source\tunifrac\tprecision\trecall\tquant_unifrac");
		else
			out.println("source\tunifrac\tprecision\trecall");
		for(String dataset : results.keySet())
			out.println(dataset + "\t" + results.get(dataset));
		out.close();

		System.out.println("Results successfully written to " + outputTabularFile);
	}

	private static HashMap<Integer,Double> readCommunity(String path) throws Exception
	{
		System.out.println("Reading community file: " + path);
		long time = System.currentTimeMillis();
		HashMap<Integer,Double> community = new HashMap<Integer,Double>();
		BufferedReader in = new BufferedReader(new FileReader(path));
		String line;
		double total = 0;
		while((line = in.readLine()) != null)
		{
			String[] words = line.split("\t");
			String name = words[0].replace("_", " ");
			Integer id = null;
			String[] subName = name.split(">");
			for(int i = subName.length-1; i >= 0 && id == null; i--)
				id = ncbi.getClass(subName[i].trim());
			if(id != null)
			{
				double count = 0.0;
				if(words.length > 1)
				{
					count = Double.parseDouble(words[1]);
					total += count;
				}
				update(community,id,count);
				for(Integer a : ncbi.getAncestors(id))
					update(community,a,count);
			}
		}
		in.close();
		for(Integer id : community.keySet())
			community.put(id, community.get(id)/total);
		time = (System.currentTimeMillis() - time)/1000;
		System.out.println("Finished in " + time + " seconds");
		System.out.println("Unique taxa: " + community.size());
		System.out.println();
		return community;
	}

	private static void update(HashMap<Integer,Double> community, int id, double count)
	{
		if(community.containsKey(id))
			community.put(id, community.get(id)+count);
		else
			community.put(id, count);
	}
}