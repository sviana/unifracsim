# UniFracSim (Unifrac Similiarity Tool)

## The purpose
This tool is intended to compute the Unifraq[^1] similarity between two samples. The Unifraq gives an idea of (dis)similarity between two communities in terms of their taxonomical composition. Instead of using this measure for quantitively providing a numerical value for the dissimilarity between two samples (in which the pure identity is equal to zero), this measure calculates the simetric, being zero when there is not even one taxon in common between the samples and one when the samples are absolutelty identical. There is two versions of Unifraq, one being unweighted (qualitative) in which the population size are not counted towards the identity and the weighted (quantitative) in which the population size matter.

## What it does
The tool takes a file or a directory and outputs to the stdout the unifrac similarity between two populations: usually the population we want to know about and an already known population, named control. The minimal options to run this tool are:
* The file with the population we want to test.
* An already kwown population used as standard or control.
* A taxonomy in the OBO format (here, we are using the NCBI taxonomy, available http://www.obofoundry.org/ontology/ncbitaxon.html).

This way, the minimal options to run this tool are suggested below

```bash
java -jar unifracsim.jar -f ${names_file} -c ${control} -t ncbitaxon.obo
```

`names_file` is a simple plain text file with a taxon name per line. `control` its the control filename wghich are also a text file with similar format and `ncbitaxon.obo` is the NCBI taxonomy in OBO format.
The `Mock1-control.txt` and `Pos_CTL-control.txt` files are examples of text files for the control.
The `Mock1-control.tsv` and `Pos_CTL-control.tsv` should be used for the weighted/quantitative mode computation.

## Command line options
```
-c
	control file (usually a .txt file, but if you want quantitative unifrac should be a .tsv instead)
-q
	quantitative unifrac
-s
	the file containing the taxon names to be compared with the control
-d
	the name of the directory containing the source files with the names,
-t
	the filename (in OBO format) of the taxonomy source (in our case, NCBI ).
```

In the `-s` option the files to be used are .txt if unweighted/qualitative Unifrac, or .tsv if quantitative (with the `-q` option).

If `-d` is used instead,  e.g., if _sample1_ is the directory name then a new .tsv named _sample1.unweighted.unifrac.tsv_ will be created with the values for each file in each TSV file line.

## Credits
The tool was mainly developed by my MSc supervisor, prof. Daniel Faria with some modifications to be able to read all the files from a directory.

## Requirements
[Apache Ant](http://ant.apache.org) is required to build and the UnifracSim tool.
It is recommended to use the Oracle JDK version 1.8 since it was used to build and test the tool, although it will work probability with any other third party JDK (e.g. OpenJDK).

## Bibliography
[^1]: Lozupone, Catherine, Knight, Rob (2005). [UniFrac: a new phylogenetic method for comparing microbial communities](http://www.pubmedcentral.nih.gov/articlerender.fcgi?artid=1317376&tool=pmcentrez&rendertype=abstract).
